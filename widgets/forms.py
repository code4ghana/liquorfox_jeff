from widgets.fields import ModelAutoCompleteField
from django import forms
from django.forms import widgets
from django.forms.widgets import flatatt
from django.utils.encoding import smart_unicode
from django.utils.html import escape
from django.utils.simplejson import JSONEncoder
from django.utils.safestring import mark_safe
from django.core import validators
from django.contrib.auth.models import User
from listings.models import Beverages
from django.contrib.auth.models import User
from django import forms
from django.utils.translation import ugettext_lazy as _

class AutocompleteForm(forms.Form):
    
    term=ModelAutoCompleteField(None,'')

    def __init__(self, model, url,*args, **kwrds):
        super(AutocompleteForm,self).__init__(*args,**kwrds)
        self.fields['term']=ModelAutoCompleteField(model,url,*args,**kwrds)
        #self.term=forms.CharField(label=name, widget=forms.TextInput(attrs={'placeholder':placeholder}))
 
    def clean(self):
        super(forms.Form,self).clean()
        return self.cleaned_data
        
    
    
    class Media:
        css = {
            'all':('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css')
            }
        js =('http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js','http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js')


class JQueryAutoComplete(forms.TextInput):
        def __init__(self, source, options={}, attrs={}):
            """source can be a list containing the autocomplete values or a
            string containing the url used for the XHR request.
            
            For available options see the autocomplete sample page::
            http://jquery.bassistance.de/autocomplete/"""
            
            self.options = None
            self.attrs = {'autocomplete': 'off'}
            self.source = source
            if len(options) > 0:
                self.options = JSONEncoder().encode(options)
            self.attrs.update(attrs)
                
        def render_js(self, field_id):
            if isinstance(self.source, list):
                source = JSONEncoder().encode(self.source)
            elif isinstance(self.source, str):
                        source = "'%s'" % escape(self.source)
            else:
                raise ValueError('source type is not valid')

            options = ''
            if self.options:
                        options += ',%s' % self.options
            return u'$(\'#%s\').autocomplete(%s%s);' % (field_id, source, options)
        
        def render(self, name, value=None, attrs=None):
            final_attrs = self.build_attrs(attrs, name=name)
            if value:
                final_attrs['value'] = escape(smart_unicode(value))
            if not self.attrs.has_key('id'):
                final_attrs['id'] = 'id_%s' % name
            return u'''<input type="text" %(attrs)s/>
        <script type="text/javascript"><!--//
        %(js)s//--></script>
        ''' % {
                'attrs' : flatatt(final_attrs),
                'js' : self.render_js(final_attrs['id']),
                }

class BeverageAutoComplete(forms.TextInput):
    class Media:
        css={'all':('css/jquery.autocomplete.css','css/thickbox.css')}
        js=('js/jquery.js','js/jquery.autocomplete.js','js/jquery.autocomplete.min.js','js/jquery.autocomplete.pack.js','js/jquery.bgiframe.min.js','js/jquery.ajaxQueue.js','js/thickbox-compressed.js')

    def __init__(self, source, options={}, attrs={}):
        """source can be a list containing the autocomplete values or a
        string containing the url used for the XHR request.
        For available options see the autocomplete sample page::
        http://jquery.bassistance.de/autocomplete/"""
        self.options = None
        self.attrs = {'autocomplete':'on'}
        self.source = source
        if len(options) > 0:
            self.options = JSONEncoder().encode(options)
        self.attrs.update(attrs)
    def render_js(self, field_id):
        if isinstance(self.source, list):
            source = JSONEncoder().encode(self.source)
        elif isinstance(self.source, str):
            source = "'%s'" % escape(self.source)
        else:
            raise ValueError('source type is not valid')
        options = ''
        if self.options:
            options += ',%s' % self.options
        return u'$(\'#%s\').autocomplete(%s%s);' % (field_id, source,
                                                        options)
    def render(self, name,value=None, attrs=None):
        final_attrs = self.build_attrs(attrs, name=name)
        lookup_attrs = self.build_attrs(attrs, name=name)
        if value:
            beverage = Beverages.objects.get(name=value)
            lookup_attrs['value'] = escape(beverage.name)
            key_value = value
            lookup_value = escape(beverage.name)
        else:
            key_value = ''
            lookup_value = ''
        if not self.attrs.has_key('id'):
            lookup_attrs['id'] = 'id_%s_lookup' % name
            final_attrs['id'] = 'id_%s' % name
        return mark_safe(u'''
            <script type="text/javascript">
                $().ready(function() {
                $("#%(field_id)s_lookup").result(function(event, data,
formatted) {
                if (data) document.getElementById("%(field_id)s").value
= data[1];
                });});
            </script>
            <input type="text" autocomplete="%(field_auto)s"
name="%(field_name)s_lookup" id="%(field_id)s_lookup"
value="%(lookup_value)s"/>
            <script type="text/javascript">
                <!--//
                %(js)s
                //-->
            </script>
            <input type="hidden" id="%(field_id)s" name="%(field_name)s"
value="%(key_value)s"/>
            ''' % {
                'key_value'     : key_value,
                'lookup_value'  : lookup_value,
                #'field'         : attrs['id'],
                'field_name'    : final_attrs['name'],
                'field_id'      : final_attrs['id'],
                'field_auto'    : final_attrs['autocomplete'],
                'js'            : self.render_js(lookup_attrs['id']),
                })

class DatePicker(forms.DateInput):
        template_name = 'datepicker.html'
        class Media:
            js = (
                '/media/js/jquery.min.js',
                '/media/js/jquery-ui.min.js',
                )


def make_custom_datefield(f):
        formfield = f.formfield()
        if isinstance(f, forms.DateField):
            formfield.widget.format = '%m/%d/%Y'
            formfield.widget.attrs.update({'class':'datePicker', 'readonly':'true'})
            return formfield
        

            

# I put this on all required fields, because it's easier to pick up
# on them with CSS or JavaScript if they have a class of "required"
# in the HTML. Your mileage may vary. If/when Django ticket #3515
# lands in trunk, this will no longer be necessary.
attrs_dict = {'class': 'required'}


class RegistrationForm(forms.Form):
    """
    Form for registering a new user account.
    
    Validates that the requested username is not already in use, and
    requires the password to be entered twice to catch typos.
    
    Subclasses should feel free to add any additional validation they
    need, but should avoid defining a ``save()`` method -- the actual
    saving of collected user data is delegated to the active
    registration backend.
    
    """

    class Meta:
        model=User

    bad_domains=[]
    formfield_callback=make_custom_datefield
    username = forms.RegexField(regex=r'^[\w.@+-]+$',
                                max_length=30,
                                widget=forms.TextInput(attrs=attrs_dict),
                                label=_("Username"),
                                error_messages={'invalid': _("This value must contain only letters, numbers and underscores.")})
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict,
                                                               maxlength=75)),
                             label=_("E-mail"))
    
    birthday=forms.DateField(label='Birthday')

    
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
                                label=_("Password"))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
                                label=_("Confirm Password"))
    tos = forms.BooleanField(widget=forms.CheckboxInput(attrs=attrs_dict),
                             label=_(u'I have read and agree to the Terms of Service'),
                             error_messages={'required': _("You must agree to the terms to register")})
                             
    def clean_birthday(self):
        from datetime import date
        dob = self.cleaned_data['birthday']
        today = date.today()
        if (dob.year + 18, dob.month, dob.day) > (today.year, today.month, today.day):
            raise forms.ValidationError('Must be at least 18 years old to register')
        return dob
        

    def isvalidDateOfBirth(self,field_data,all_data):
        from datetime import date
        dob = field_data
        today = date.today()
        if (dob.year + 18, dob.month, dob.day) > (today.year, today.month, today.day):
            raise forms.ValidationError('Must be at least 18 years old to register')
        return 
    
    def clean_username(self):
        """
        Validate that the username is alphanumeric and is not already
        in use.
        
        """
        try:
            User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_("A user with that username already exists."))
    
    def clean(self):
        """
        Verifiy that the values entered into the two password fields
        match. Note that an error here will end up in
        ``non_field_errors()`` because it doesn't apply to a single
        field.
        
        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields didn't match."))
            return self.cleaned_data

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        
        """

        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use. Please supply a different email address."))
        email_domain = self.cleaned_data['email'].split('@')[1]
        if email_domain in self.bad_domains:
            raise forms.ValidationError(_("Registration using free email addresses is prohibited. Please supply a different email address."))
        return self.cleaned_data['email']

