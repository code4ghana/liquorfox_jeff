from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from dajaxice.core import dajaxice_autodiscover
#from ajax_select import urls as ajax_select_urls
admin.autodiscover()
dajaxice_autodiscover()


urlpatterns=patterns('',
                     #temporary home page to show that this works
#                     url(r'',include('social_auth.urls')),
#                     (r'^feedback/', include('feedback.urls')),
#                     (r'^admin/lookups/', include(ajax_select_urls)),
                     (r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
                     url(r'^fake/',direct_to_template,{'template':'landingpage/test.html'},name='fakeit'),
                     url(r'^$','listings.views.landing',name='landing'),
                     url(r'^main/','listings.views.main',name='main'),
                     url(r'^main/drinks_search/','listings.views.ajax_beverage',name='drinks_search'),
                     url(r'^maintenance/',direct_to_template, {'template': 'landingpage/construction.html'}),
                     (r'^accounts/', include('userena.urls')),
                     (r'^messages/', include('userena.contrib.umessages.urls')),
                     (r'^admin/doc/', include('django.contrib.admindocs.urls')),                     
                     url(r'^admin/', include(admin.site.urls)),
                     )
if settings.DEBUG:
    urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

