# Django settings for liquorfox project.
import os

    
ROOT = lambda base : os.path.join(os.path.dirname(__file__), base).replace('\\','/')
#settings that I defined
ACCOUNT_ACTIVATION_DAYS = 4
DEFAULT_FROM_EMAIL = 'admin@liquorfox.com'
EMAIL_HOST='smtp.webfaction.com'
EMAIL_USE_TLS=1
EMAIL_PORT=587
EMAIL_HOST_USER='liquorfox'
EMAIL_HOST_PASSWORD='readinganddrinkingemails'
SERVER_EMAIL='server@liquorfox.com'
# Automatically log the user in.
AUTH_PROFILE_MODULE = 'profiles.Profile'


#----------maintenance-----mode------------
MAINTENANCE=True
LOGIN_REDIRECT_URL = '/accounts/%(username)s/'
LOGIN_URL = 'accounts/signin/'
LOGOUT_URL = 'accounts/signup/'
ADMIN_LOGIN_URL = '/admin/'
ADMIN_LOGOUT_URL = '/admin/logout/'
MAINTENANCE_PATH = '/maintenance/'
#-----------maintenance----end------------


DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Jeff','jeffkusi@gmail.com')
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'liquorfox_db',                      # Or path to database file if using sqlite3.
        'USER': 'liquorfox_db',                      # Not used with sqlite3.
        'PASSWORD': 'b943f1d2',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = '/home/liquorfox/webapps/mediaserver/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = 'http://www.liquorfox.com/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/home/liquorfox/webapps/staticserver/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = 'http://www.liquorfox.com/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = 'http://www.liquorfox.com/admin/media/'

# Additional locations of static files
STATICFILES_DIRS = (
    '/home/liquorfox/webapps/django/liquorfox/static',
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'i4_!9e!c3dlm(p-h0vs=77aa8&f*egp*(%*_1&+4j097o)+b^^'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',

)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'middleware.LoginFormMiddleware',
    'middleware.MaintenanceMiddleware',
)

ROOT_URLCONF = 'liquorfox.urls'

TEMPLATE_DIRS = (
    ROOT('templates'),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:,
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:,
    'django.contrib.admindocs',
    'listings',
    'profiles',
    #third party apps,
    'userena',
    'userena.contrib.umessages',
    'guardian',
    'easy_thumbnails',
    'dajaxice',
    #'feedback',
    #'ajax_select',
    
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
            },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'simple'
            },
        },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            },
        'simple': {
            'format': '%(levelname)s %(message)s'
            },
        },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django':{
            'handlers':['console'],
            'propagate':True,
            'level':'INFO',
            },

    }
}



from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS,AUTHENTICATION_BACKENDS
#TEMPLATE_CONTEXT_PROCESSORS += ('listings.contextProcessors.loginForm',)

#-----------------------userena settings--------
AUTHENTICATION_BACKENDS += (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    )
ANONYMOUS_USER_ID=-1

#-------------END of userena settings----------

#------------dajaxice settings-----------
TEMPLATE_LOADERS+=(
        'django.template.loaders.eggs.Loader',
    )
TEMPLATE_CONTEXT_PROCESSORS+=(
    'django.core.context_processors.request',
    )
DAJAXICE_MEDIA_PREFIX="dajaxice"
#------------END of dajaxice-------------

#------------feedback settings-----------

#------------END of feedback-------------
#------------ajax-selects settings--------
AJAX_LOOKUP_CHANNELS = {
        #   pass a dict with the model and the field to search against
        'beverage'  : {'model':'listings.Beverages', 'search_field':'name'}
        }
AJAX_SELECT_BOOTSTRAP = True
AJAX_SELECT_INLINES = 'inline'
#------------END of ajax-selects-----------------

try:
    import logging
    import logging.config
    logger = logging.getLogger('dajaxice')
    logger.setLevel(logging.DEBUG)
    hdlr = logging.FileHandler('debug_dajaxice.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    from local_settings import *
except IOError as (errno, strerror):
        print "I/O error({0}): {1}".format(errno, strerror)
except:
    print 'something strange happened in loading of extra settings'
    pass
