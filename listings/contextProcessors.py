def loginForm(request):
    from django.contrib.auth.forms import AuthenticationForm
    """Used by settings.TEMPLATE_CONTEXT_PROCESSORS to provide an item count
    to every template"""
    login_form=AuthenticationForm()
    return {'login_form':login_form}
