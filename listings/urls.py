from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('listings.views',
                       url(r'^ajax_beverages_from_home', 'ajax_beverages_list',name='ajax_beverages_from_home'),
                       url(r'^ajax_beverage/$','ajax_beverage'),
                       url(r'^(?P<seller_id>\d+)/$', 'seller'),
                       url(r'^(?P<beverage_id>\d+)/$', 'beverage_id'),
                       url(r'^(?P<beverage_name>\w+)/$', 'beverage'),
                       #url(r'^beverage_search/$','beverage_ajax')
                   
                       )
