from listings.models import Beverages, Bars, Stores, Votes, Votables, Sells, Sellers
from django.contrib import admin
admin.site.register(Sellers)
admin.site.register(Votables)
admin.site.register(Beverages)
admin.site.register(Bars)
admin.site.register(Stores)
admin.site.register(Votes)
admin.site.register(Sells)

    
