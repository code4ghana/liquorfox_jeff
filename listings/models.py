from django.db import models


from profiles.models import Profile

class IntegerRangeField(models.IntegerField):
	def __init__(self,min_value=None, max_value=None, **kwargs):
		self.min_value, self.max_value = min_value, max_value
		models.IntegerField.__init__(self, **kwargs)
		
	def formfield(self, **kwargs):
		defaults = {'min_value': self.min_value, 'max_value':self.max_value}
		defaults.update(kwargs)
		return super(IntegerRangeField, self).formfield(**defaults)
        
class Beverages(models.Model):
	class Meta:
		verbose_name_plural='Baverages'
		verbose_name='Beverage'

	name=models.CharField(max_length=100)
	origin=models.CharField(max_length=100,blank=True)
	color=models.CharField(max_length=30,blank=True)
	pic_url=models.URLField()
	description=models.TextField(blank=True)
	type1=models.CharField( max_length=100)
	type2=models.CharField( blank=True, max_length=100)
	phase1=models.CharField(max_length=10,blank=True)
	phase2=models.CharField(max_length=10, blank=True)
	proof=IntegerRangeField(verbose_name='proof',max_value=100)
	alcohol=IntegerRangeField(verbose_name='alcohol', max_value=100)
	min_age=models.IntegerField(default=0)
	max_age=models.IntegerField(default=0)
	distery_location=models.CharField(max_length=100,blank=True)
	popular_mix1=models.TextField('popular_mix1', blank=True)
	popular_mix2=models.TextField('popular_mix2', blank=True)
	parent_company=models.CharField(max_length=100, blank=True)
	

	def __unicode__(self):
		return self.name

	def is_available(self):
		return Sells.objects.filter(beverage__name__exact=self.name).exists()

	def asJson(self,ALL=True, **kwargs):
		if ALL:
			dump={'name':self.name,
			      'origin':self.origin,
			      'color':self.color,
			      'pic':self.pic_url,
			      'description':self.description,
			      'type1':self.type1,
				}
			return dump
		
		dump={}
		if 'name' in kwargs:
			dump['name']=self.name
		if 'origin' in kwargs:
			dump['origin']=self.origin
		if 'color' in  kwargs:
			dump['color']=self.color
		if 'pic' in kwargs:
			dump['pic']=self.pic_url
		if 'description' in kwargs:
			dump['description']=self.description
		if 'type1' in kwargs:
			dump['type1']=self.type1
		return dump
	
class Sellers(models.Model):
	class Meta:
		verbose_name_plural='Sellers'
		verbose_name='Seller'


	
class Votables(models.Model):
	class Meta:
		verbose_name_plural='Votables'
		verbose_name='Votable'



class Bars(Sellers,Votables):
	
	class Meta:
		verbose_name_plural='Bars'
		verbose_name='Bar'

	name=models.CharField(max_length=100)
	email=models.CharField(max_length=50)
	music=models.CharField(max_length=100)
	crowd=models.CharField(max_length=50)

	def __unicode__(self):
		return self.name


class Stores(Sellers):
	
	class Meta:
		verbose_name_plural='Stores'
		verbose_name='Store'
		
	name=models.CharField(max_length=100)
	email=models.CharField(max_length=50)
	music=models.CharField(max_length=100)
	crowd=models.CharField(max_length=50)

	def __unicode__(self):
		return self.name
	

class Votes(models.Model):
	class Meta:
		verbose_name_plural='Votes'
		verbose_name='Vote'

	voter=models.ForeignKey(Profile)
	item=models.ForeignKey('Votables')
	VOTE_CATHEGORIES=(
		('B', 'Bars'),
		('S', 'Stores'),
		)
	cat=models.CharField(max_length=1,choices=VOTE_CATHEGORIES)
	vote=IntegerRangeField(default=5,max_value=10)
	

class Sells(models.Model):
	class Meta:
		verbose_name_plural='Selling'
		verbose_name='Selling'
		
	seller=models.ForeignKey('Sellers')
	beverage=models.ForeignKey('Beverages')
	price=models.DecimalField(max_digits=10,decimal_places=2)
	size=models.IntegerField()

	def __unicode__(self):
		return "%s sells %s for $%d" %(self.seller.name,self.beverage.name,self.price)
